import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import Card from './Card';
import Colors from '../constants/color';

const ConfirmedInput = props => {
  return (
    <Card style={{ marginTop: 20 }}>
      <View style={styles.container}>
        <Text style={styles.title}>
          You Selected
        </Text>
        <View style={styles.numberContainer}>
          <Text style={styles.number}>{props.confirmedValue}</Text>
        </View>
        <View>
          <Button title="Start Game" onPress={props.startGame} />
        </View>
      </View>
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  title: {
    fontSize: 20
  },
  numberContainer: {
    marginVertical: 10,
    borderColor: Colors.accent,
    borderRadius: 10,
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  number: {
    fontSize: 15,
    color: Colors.accent,
  }
});

export default ConfirmedInput;