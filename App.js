import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';

const App = () => {
  const [selectedNumber, setUserNumer] = useState(0);

  const startGameHandler = (number) => {
    setUserNumer(number);
  }

  return (
    <View style={styles.screen}>
      <Header title='Guess A Number' />
      {selectedNumber
        ? <GameScreen userChoice={selectedNumber} />
        : <StartGameScreen startGameHandler={startGameHandler} />}
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1
  }
});

export default App;
