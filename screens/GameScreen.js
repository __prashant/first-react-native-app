import React, { useState } from 'react';
import { View, Text, Button, StyleSheet, Alert } from 'react-native';
import Card from '../components/Card';

const guess = (min, max, exclude) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  let currGuess = Math.floor(Math.random() * (max - min)) + min;

  if (currGuess === exclude) {
    return guess(min, max, exclude);
  }

  return currGuess;
}

let max = 99;
let min = 0;

const GameScreen = (props) => {
  const [currentGuess, setGuess] = useState(guess(min, max, props.userChoice));

  const hasWon = (number) => {
    return number === props.userChoice;
  }

  const nextGuessHandler = direction => {
    if ((direction === 'lower' && currentGuess < props.userChoice) ||
      (direction === 'higher' && currentGuess > props.userChoice)) {
      return Alert.alert('Cheating', 'Please dont cheat', [{
        text: 'Sorry',
        style: 'destructive'
      }]);
    }

    if (direction === 'lower') {
      max = parseInt(currentGuess);
    } else {
      min = parseInt(currentGuess);
    }

    console.log(min);
    console.log(max);

    let newGuess = guess(min, max);
    setGuess(newGuess);

    if (hasWon(newGuess)) {
      return Alert.alert('Yaaaaaay', 'CPU Won', [{
        text: 'Awesome',
        style: 'destructive'
      }]);
    }

  }

  return (
    <View style={styles.screen}>
      <Text>Computers Guess</Text>
      <Text>{currentGuess}</Text>
      <Card style={styles.buttonContainer}>
        <Button title="Lower" onPress={nextGuessHandler.bind(this, 'lower')} />
        <Button title="Higher" onPress={nextGuessHandler.bind(this, 'higher')} />
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    width: 300,
    maxWidth: '80%'
  }
});

export default GameScreen;