import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback, Button, Keyboard, Alert } from 'react-native';
import Card from '../components/Card';
import Colors from '../constants/color';
import Input from '../components/Input.js';
import ConfirmedInput from '../components/ConfirmedInput';

const StartGameScreen = props => {

  const [enteredValue, setvalue] = useState('');
  const [confirmed, setConfirm] = useState(false);
  const [confirmedValue, setConfirmValue] = useState('');

  const inputHandler = (text) => {
    setvalue(text.replace(/[^0-9]/g, ''));
  }

  const resetUserInput = () => {
    setvalue('');
    setConfirm(false);
    setConfirmValue('');
  }

  const confirmUserInput = () => {
    const chosenNumber = parseInt(enteredValue);

    if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
      Alert.alert('Invalid Number', 'Number has to be between 1 - 99', [{
        text: 'Okay',
        style: 'destructive',
        onPress: resetUserInput
      }]);
      return;
    }

    setConfirm(true);
    setConfirmValue(chosenNumber);
    setvalue('');
    Keyboard.dismiss();
  }

  const startGame = () => {
    props.startGameHandler(confirmedValue);
  }

  return (
    <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
      <View style={styles.screen}>
        <Text style={styles.title}>Start A New Game</Text>
        <Card>
          <View>
            <Text>Select A Number</Text>
            <Input
              style={styles.input}
              blurOnSubmit
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="numeric"
              maxLength={2}
              onChangeText={inputHandler}
              value={enteredValue}
            />
          </View>
          <View style={styles.buttonContainer} >
            <View style={styles.button}>
              <Button style={styles.button} title="Reset" onPress={resetUserInput} color={Colors.accent} />
            </View>
            <View style={styles.button}>
              <Button style={styles.button} title="Confirm" onPress={confirmUserInput} color={Colors.primary} />
            </View>
          </View>
        </Card>
        {confirmed && <ConfirmedInput confirmedValue={confirmedValue} startGame={startGame} />}
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 5,
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    marginVertical: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingHorizontal: 15
  },
  button: {
    width: '40%'
  },
  input: {
    textAlign: 'center'
  }
});

export default StartGameScreen;